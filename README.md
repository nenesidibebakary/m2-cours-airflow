---
output:
  pdf_document: default
---

# Documentation Technique du Flux Airflow


## Introduction
Ce document décrit le flux de traitement des données avec Airflow, depuis l'entrée des données jusqu'à la sortie finale. Il est destiné aux data engineers, data scientists, data analysts et administrateurs de systèmes. Les données sont traitées chaque mois.


## Table des Matières
1. [Présentation Générale du Flux](#présentation-générale-du-flux)
2. [Détails des Étapes du Flux](#détails-des-étapes-du-flux)
   - [Input](#input)
   - [Nettoyage des Données](#nettoyage-des-données)
   - [Tables de Rejets](#tables-de-rejets)
   - [Agrégation](#agrégation)
   - [Fusion Finale](#fusion-finale)
3. [Gestion des Logs](#gestion-des-logs)
4. [Résultats et Output](#résultats-et-output)
5. [Configuration et Déploiement](#configuration-et-déploiement)
6. [Annexes](#annexes)
7. [Références](#références)

## Présentation Générale du Flux
Le diagramme ci-dessous illustre le flux global de traitement des données :

![Diagramme du Flux](Compte_Rendu_TP_Airflow/dags/documentations_workflow.png)

## Détails des Étapes du Flux

### Input
#### Données à disposition - description
Les dossiers nommés ***230101***, ***230201*** et ***230301*** contiennent les exports respectivement reçus les premier janvier, févirer et mars 2023 et portant sur le mois précédent. Ils contiennent chacun trois fichiers : ***patient_xxxxxx.csv, sejour_xxxxxx.csv*** et ***medicaments_xxxxxx.csv*** 
Les formats attendus sont :
- **Médicaments** : CSV
- **Séjours** : CSV
- **Patients** : CSV

### Nettoyage des Données
Cette étape comprend plusieurs sous-processus :
- **Formatage**
- **Gestion des séjours chevauchants**
- **Identification des séjours**
- **Interventions hors intervalles**
- **Valeurs anormales de posologie**
- **Suppression des doublons**  

Scripts utilisés : `data_management.py`  
Logs :
- dossier : logs
   - logs_xcom  
Résultat :
- dossier : clean_all_files_export
   - 230301 (dernier mois traité)
      - medicament_230301_formated
      - medicament_230301_formated_drop_dupliq
      - sejour_230301_formated
- dossier : Rejected
   - rejected_230301_doublons_medic 
   - rejected_230301_doublons_sej

### Agrégation
Les données nettoyées sont ensuite agrégées.

#### Variables calculées

| **nom** | **Signification** |
|---|---|
| duree_reveil_tot | Durée totale passée en réveil au cours du séjour |
| duree_rea_tot | Durée totale passée en réa au cours du séjour|
| nb_medic | Nombre de médicaments différents administrés au cours du séjour |

Scripts utilisés : `aggregate.py`  
Logs :
- dossier : logs
   - logs_xcom_aggr  
Résultat :
- dossier : clean_monthly_export
   - rea_month_230301 (dernier mois agrégé)

### Fusion Finale
Les données agrégées sont fusionnées avec le dernier fichier agrégé.
Scripts utilisés : `merged_data.py`  
Logs :
- dossier : logs
   - logs_xcom_merged  
Résultat :
- dossier : latest_full_export
   - rea_full_230301 (dernier mois traité)


## Configuration et Déploiement
### Configuration Airflow
Configurer les paramètres suivants dans votre environnement Airflow :
- `dag_id`
- `schedule_interval`
- `default_args`

## Description du DAG

## Configuration du DAG

- **Nom du DAG** : `TP_airflow_last_version`
- **Description** : Ce DAG illustre la gestion des tâches avec journalisation intégrée, la gestion des erreurs, et l'utilisation des XCom pour le transfert d'informations. Il exécute des scripts de gestion, d'agrégation et de fusion de données, tout en enregistrant les logs à chaque étape et en déclenchant des tâches en cas d'échec.
- **Intervalle de planification** :  Tous les 30 jours (timedelta(days=30))
- **Date de début** : 9 mai 2024
- **Pas de rattrapage des exécutions manquées** (`catchup = False`)
- **Le DAG est initialement en pause** (`is_paused_upon_creation = True`)

## Structure du DAG

![Overview dag](captures_tp_logs/overview_airflow.png)

1. `lister_contenu` (BashOperator) :
   - Liste le contenu du répertoire spécifié.

2. `execute_DataManagement` (BashOperator) :
   - Exécute le script `data_management.py` et enregistre l'heure de début dans un XCom.

3. `xcom_execute_DM` (BashOperator) :
   - Exécute le script de journalisation des erreurs en cas d'échec de la tâche précédente.

4. `first_log_DM` (PythonOperator) :
   - Exécute la fonction `first_log` pour enregistrer les logs des fichiers de données nettoyés et rejetés.
   ![Premier log](captures_tp_logs/first_log.png)

5. `execute_aggregate` (BashOperator) :
   - Exécute le script `aggregate.py` et enregistre l'heure de début dans un XCom.

6. `xcom_execute_aggr` (BashOperator) :
   - Exécute le script de journalisation des erreurs en cas d'échec de la tâche précédente.

7. `execute_merged_files` (BashOperator) :
   - Exécute le script `merged_data.py` et enregistre l'heure de début dans un XCom.

8. `xcom_execute_merged` (BashOperator) :
   - Exécute le script de journalisation des erreurs en cas d'échec de la tâche précédente.

9. `second_log_merged` (PythonOperator) :
   - Exécute la fonction `second_log` pour enregistrer les logs du dernier export complet des données.
   ![Deuxieme log](captures_tp_logs/second_log.png)

## Arborescence des Fichiers

L'arborescence des fichiers du projet est la suivante :

![Arborescence](Compte_Rendu_TP_Airflow/dags/tree.png)

## Exécution
Pour exécuter ce DAG :

1. Assurez-vous que les chemins de fichiers et les scripts référencés existent sur votre système Airflow.
2. Activez ce DAG depuis l'interface utilisateur d'Airflow.
3. Observez les logs générés pour chaque tâche pour vérifier le bon fonctionnement.


### Déploiement
Pour déployer le flux, suivez ces étapes :
1. Placer les scripts dans le répertoire `dags` d'Airflow.
2. Configurer les connexions et variables nécessaires.
3. Démarrer le service Airflow.

## Annexes
### Glossaire
- **Séjours chevauchants** : Séjours qui se chevauchent dans le temps.
- **Valeurs anormales de posologie** : Valeurs de dosage qui sortent des plages normales.

### Scripts
- `datamanagement.py`
- `aggregate.py`
- `merged_data.py`

## Références
- [Documentation Airflow](https://airflow.apache.org/docs/)
