from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime, timedelta

from airflow.utils.trigger_rule import TriggerRule


with DAG(
    'exemple_logs_trigger_condition',
    description="Exemple de récupération des logs directement au sein de la tâche + Tâche en cas d'erreur",
    schedule_interval = None,
    start_date=datetime(2024, 5, 9),
    catchup = False,
    is_paused_upon_creation = True

) as dag:

    
    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='ls ')


    t2 = BashOperator(
        task_id='execute_python',
        bash_command="python3 /opt/airflow/dags/scripts_autres/script_logs_trigger_condition.py")

    t3 = BashOperator(
        task_id="echec_execute_python",
        trigger_rule="all_failed",
        bash_command ="python3 /opt/airflow/dags/scripts_autres/failed_logs_trigger_condition.py"        )


    t1 >>  t2 >> t3