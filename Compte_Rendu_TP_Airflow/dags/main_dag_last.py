from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python_operator import PythonOperator
import logging
from datetime import datetime, timedelta

from airflow.utils.trigger_rule import TriggerRule


def get_time():
    return(str(datetime.now()))

with DAG(
    'TP_airflow_last_version',
    description="Ce DAG illustre la gestion des tâches avec journalisation intégrée, la gestion des erreurs, et l'utilisation des XCom pour le transfert d'informations. Il exécute des scripts de gestion, d'agrégation et de fusion de données, tout en enregistrant les logs à chaque étape et en déclenchant des tâches en cas d'échec.",
    schedule_interval = timedelta(days = 30),
    start_date=datetime(2024, 5, 9),
    catchup = False,
    is_paused_upon_creation = True

) as dag:



    
    # Define logs

    def first_log(): 
        file1_path = "/opt/airflow/dags/DEV/output/clean_monthly_export/rea_month_230301.csv"
        file2_path = "/opt/airflow/dags/DEV/output/clean_all_files_export/230301/medicament_230301_formated.csv"
        file3_path = "/opt/airflow/dags/DEV/output/clean_all_files_export/230301/sejour_230301_formated.csv"
        file4_path = "/opt/airflow/dags/DEV/input/230301/patient_230301.csv"
        file5_path = "/opt/airflow/dags/DEV/output/Rejected/rejected_230301_doublons_medic.csv"
        file6_path = "/opt/airflow/dags/DEV/output/Rejected/rejected_230301_doublons_sej.csv"

        with open(file1_path) as file1:
            num_lines_file1 = sum(1 for line in file1)
        with open(file2_path) as file2:
            num_lines_file2 = sum(1 for line in file2)
        with open(file3_path) as file3:
            num_lines_file3 = sum(1 for line in file3)
        with open(file4_path) as file4:
            num_lines_file4 = sum(1 for line in file4)
        with open(file5_path) as file5:
            num_lines_file5 = sum(1 for line in file5)
        with open(file6_path) as file6:
            num_lines_file6 = sum(1 for line in file6)

        logging.info("Here is some information about the last rea_month saved.")
        logging.info("::group::Nombre de lignes insérées pour le dernier fichier rea")
        logging.info(f"rea_month {num_lines_file1} lignes insérées")
        logging.info("::endgroup::")
        logging.info("Logs about the other files")
        logging.info("::group::Nombre de lignes insérées de chaque fichier nettoyé en input")
        logging.info(f"Medicament {num_lines_file2} lignes insérées")
        logging.info(f"Sejour {num_lines_file3} lignes insérées")
        logging.info(f"Patient {num_lines_file4} lignes insérées")
        logging.info("::endgroup::")
        logging.info("Logs about rejected lines")
        logging.info("::group::Nombre de lignes rejetées de chaque fichier nettoyé en input")
        logging.info(f"Medicament {num_lines_file5} lignes rejetées")
        logging.info(f"Sejour {num_lines_file6} lignes rejetées")
        logging.info("::endgroup::")


    def second_log():
        file_export_path = "/opt/airflow/dags/DEV/output/latest_full_export/rea_full_230301.csv"
        with open(file_export_path) as file_export:
            num_lines_file_export = sum(1 for line in file_export)
            logging.info("Logs about the last full export of rea")
            logging.info("::group::Nombre de lignes total du dernier mois de full export en rea")
            logging.info(f"rea_full_230301 {num_lines_file_export} lignes totaux")
            logging.info("::endgroup::")


    
    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='ls ')



    t2 = BashOperator(
        task_id='execute_DataManagement',
        bash_command="echo '{{ ti.xcom_push(key='start', value='" + str(datetime.now()) + 
            "') }}' ; python3 /opt/airflow/dags/Scripts/data_management.py" ,
        do_xcom_push = True)



    xcomdm = BashOperator(
        task_id="xcom_execute_DM",
        trigger_rule="all_done",
        bash_command ="python3 /opt/airflow/dags/Scripts/failed_logs_xcom.py '{{ti.xcom_pull(key='start')}}'"  
        )


    t3 = BashOperator(
        task_id='execute_aggregate',
        bash_command="echo '{{ ti.xcom_push(key='start', value='" + str(datetime.now()) + 
            "') }}' ; python3 /opt/airflow/dags/Scripts/aggregate.py" ,
        do_xcom_push = True)
    


    xcomaggr = BashOperator(
        task_id="xcom_execute_aggr",
        trigger_rule="all_done",
        bash_command ="python3 /opt/airflow/dags/Scripts/failed_logs_xcom_aggregate.py '{{ti.xcom_pull(key='start')}}'"  
        )



    t4 = BashOperator(
        task_id='execute_merged_files',
        bash_command="echo '{{ ti.xcom_push(key='start', value='" + str(datetime.now()) + 
            "') }}' ; python3 /opt/airflow/dags/Scripts/merged_data.py" ,
        do_xcom_push = True)
    
    


    xcommerg = BashOperator(
        task_id="xcom_execute_merged",
        trigger_rule="all_done",
        bash_command ="python3 /opt/airflow/dags/Scripts/failed_logs_xcom_merged.py '{{ti.xcom_pull(key='start')}}'"  
        )


    
    first_log_task = PythonOperator(
        task_id='first_log_DM',
        python_callable=first_log,
        dag=dag,
    )


    second_log_task = PythonOperator(
        task_id='second_log_merged',
        python_callable=second_log,
        dag=dag,
    )






    t1 >>  t2  >> xcomdm >> first_log_task >>  t3 >> xcomaggr >> t4 >> xcommerg >> second_log_task
