# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:33:49 2024

@author: Nene sidibe BAKARY
"""

import pandas as pd 
from dateutil.parser import parse
import os
from pathlib import Path
from datetime import datetime


# Date de début de tâche
start = datetime.now()


# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/logs/logs_xcom.csv") :

	logs = pd.read_csv("/opt/airflow/dags/logs/logs_xcom.csv", sep = ";")

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[],
		"status" : [],
        "Nb_lines_inserted_med" : [],
        "Nb_lines_inserted_sej" : [],
        "Nb_lines_inserted_pat" : [],
        "Nb_lines_rejected_doublons_medic" : [],
        "Nb_lines_rejected_doublons_sej" : [],
        "Nb_lines_rejected_doublons_pat" : []
    })
	

# Tâche principale

############################################################################################################################################################################################################################

# environnements
chemin_input = "/opt/airflow/dags/DEV/input/"

# data load
max_folder = max([name for name in os.listdir(chemin_input)]) # select the last folder
pathlist = list(Path(chemin_input +"/"+ max_folder).glob("**/*.csv")) # list all files from this folder
data_dict = {filepath.name.split(".csv")[0]:pd.read_csv(filepath, sep=";") for filepath in pathlist } # create a dictionnary with file_nam as key and data of each of them as values
# files lecture
medicament = data_dict["".join([key for key in data_dict.keys() if key.startswith("medicament")])]
sejour = data_dict["".join([key for key in data_dict.keys() if key.startswith("sejour")])]
patient = data_dict["".join([key for key in data_dict.keys() if key.startswith("patient")])]
    
 
##############################################################################################################
##### Sejour
###################
# 1 ligne = 1 séjour

# New dataframe with date formated
sejour_formated = sejour

# Initialisation du DataFrame de rejets
rejected_df_sej_date = pd.DataFrame(columns=['date_string'])
#  errors='raise'
# Fonction de conversion utilisant pd.to_datetime et fallback avec dateutil.parser.parse
def convert_to_datetime_sej(df,rejected_df):
    columns_names=['ENTREE_REVEIL', 'SORTIE_REVEIL','ENTREE_REA','SORTIE_REA']
    for col in columns_names :
        try:
            # Essayer d'abord avec pd.to_datetime
            df[col]= pd.to_datetime(df[col], format = "%d/%m/%Y %H:%M:%S" )
        except Exception:
            # En cas d'échec, utiliser dateutil.parser.parse
            try:
               df[col]=df[col].apply(parse)
            except Exception as e:
                print(f"Erreur de conversion pour {df[col]}: {e}")
                # Ajouter la ligne rejetée au DataFrame de rejets
                rejected_df.loc[len(rejected_df)] = [df[col]]
    return df, rejected_df
    
convert_to_datetime_sej(sejour_formated,rejected_df=rejected_df_sej_date)

# Calcul des durées
sejour_formated["duree_reveil_tot"] = sejour_formated["SORTIE_REVEIL"] - sejour_formated["ENTREE_REVEIL"]
sejour_formated["duree_rea_tot"] = sejour_formated["SORTIE_REA"] - sejour_formated["ENTREE_REA"]

# identification des doublons
duplicates_id_sej = sejour_formated['ID_SEJOUR'][sejour_formated['ID_SEJOUR'].duplicated()]
# Suppression des doublons de la colonne 'ID_PRESCRIP_DROGUES'
sejour_formated_drop_dupliq = sejour_formated
sejour_formated_drop_dupliq['ID_SEJOUR'] = sejour_formated_drop_dupliq['ID_SEJOUR'].drop_duplicates()

# Suppression des doublons de la colonne 'ID_PRESCRIP_DROGUES'
rejected_df_doublons_sej = sejour_formated[sejour_formated['ID_SEJOUR'].isin(duplicates_id_sej)]

i = max_folder

# saved 
sejour_formated_drop_dupliq.to_csv(f"/opt/airflow/dags/DEV/output/clean_all_files_export/{i}/sejour_{i}_formated.csv", sep=";", index= False)
rejected_df_doublons_sej.to_csv(f"/opt/airflow/dags/DEV/output/Rejected/rejected_{i}_doublons_sej.csv", sep=";", index= False)


##############################################################################################################
##### Medicaments
###################

# Structure
    
medicament_formated = medicament # copy data
rejected_df_medoc_date = pd.DataFrame(columns=['date_string']) # creation df de rejet

# Fonction de conversion utilisant pd.to_datetime et fallback avec dateutil.parser.parse
def convert_to_datetime_med(df,rejected_df):
    columns_names =['HEURE1', 'HEURE2','HEURE3']
    for col in columns_names :
        try:
            # Essayer d'abord avec pd.to_datetime
            df[col]= pd.to_datetime(df[col],  format = "%d/%m/%Y %H:%M:%S" )
        except Exception:
            # En cas d'échec, utiliser dateutil.parser.parse
            try:
               df[col]=df[col].apply(parse)
            except Exception as e:
                print(f"Erreur de conversion pour {df[col]}: {e}")
                # Ajouter la ligne rejetée au DataFrame de rejets
                rejected_df.loc[len(rejected_df)] = [df[col]]
    return df, rejected_df

convert_to_datetime_med(medicament_formated,rejected_df=rejected_df_medoc_date)

# identification des doublons
duplicates_id_prescip = medicament_formated['ID_PRESCRIP_DROGUES'][medicament_formated['ID_PRESCRIP_DROGUES'].duplicated()]
# enregistrement des lignes dupliquées dans une table de rejet
rejected_df_doublons_medic = medicament_formated[medicament_formated['ID_PRESCRIP_DROGUES'].isin(duplicates_id_prescip)]

# Suppression des doublons de la colonne 'ID_PRESCRIP_DROGUES'
medicament_formated_drop_dupliq = medicament_formated
medicament_formated_drop_dupliq['ID_PRESCRIP_DROGUES'] = medicament_formated_drop_dupliq['ID_PRESCRIP_DROGUES'].drop_duplicates()


# saved 
medicament_formated_drop_dupliq.to_csv(f"/opt/airflow/dags/DEV/output/clean_all_files_export/{i}/medicament_{i}_formated.csv", sep=";", index= False)
#medicament_formated_drop_dupliq.to_csv(f"/opt/airflow/dags/DEV/output/clean_all_files_export/230301/medicament_{i}_formated_drop_dupliq.csv", sep=";", index= False)
rejected_df_doublons_medic.to_csv(f"/opt/airflow/dags/DEV/output/Rejected/rejected_{i}_doublons_medic.csv", sep=";", index= False)



############################################################################################################################################################################################################################
##### Patients
###################

# Structure
    
patient_formated = patient # copy data

# identification des doublons
duplicates_id_patient = patient_formated['ID_PATIENT'][patient_formated['ID_PATIENT'].duplicated()]
# enregistrement des lignes dupliquées dans une table de rejet
rejected_df_doublons_patient = patient_formated[patient_formated['ID_PATIENT'].isin(duplicates_id_patient)]

# Suppression des doublons de la colonne 'ID_PRESCRIP_DROGUES'
patient_formated_drop_dupliq = patient_formated
patient_formated_drop_dupliq['ID_PATIENT'] = patient_formated_drop_dupliq['ID_PATIENT'].drop_duplicates()


# saved 
patient_formated_drop_dupliq.to_csv(f"/opt/airflow/dags/DEV/output/clean_all_files_export/{i}/patient_{i}_formated.csv", sep=";", index= False)
#medicament_formated_drop_dupliq.to_csv(f"/opt/airflow/dags/DEV/output/clean_all_files_export/230301/medicament_{i}_formated_drop_dupliq.csv", sep=";", index= False)
rejected_df_doublons_patient.to_csv(f"/opt/airflow/dags/DEV/output/Rejected/rejected_{i}_doublons_patient.csv", sep=";", index= False)


############################################################################################################################################################################################################################


end = datetime.now()

Nb_lines_inserted_med = len(medicament_formated_drop_dupliq)
Nb_lines_inserted_sej = len(sejour_formated_drop_dupliq)
Nb_lines_inserted_pat = len(patient_formated_drop_dupliq)
Nb_lines_rejected_doublons_sej = len(rejected_df_doublons_sej)
Nb_lines_rejected_doublons_medic = len(rejected_df_doublons_medic)
Nb_lines_rejected_doublons_pat = len(rejected_df_doublons_patient)

# Logs de l'exécution
new_logs = pd.DataFrame({
	"task_id" : ["execute_DataManagement"],
	"start" : [start],
	"end" : [end],
	"status" : ["succes"],
    "Nb_lines_inserted_med" : Nb_lines_inserted_med,
    "Nb_lines_inserted_sej" : Nb_lines_inserted_sej,
    "Nb_lines_inserted_pat" : Nb_lines_inserted_pat,
    "Nb_lines_rejected_doublons_medic" : Nb_lines_rejected_doublons_medic,
    "Nb_lines_rejected_doublons_sej" : Nb_lines_rejected_doublons_sej,
    "Nb_lines_rejected_doublons_pat" : Nb_lines_rejected_doublons_pat
    })

# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])

# Exports
all_logs.to_csv("/opt/airflow/dags/logs/logs_xcom.csv",
	sep = ';', index = False)




