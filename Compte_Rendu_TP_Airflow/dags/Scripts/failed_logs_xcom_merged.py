import os
import pandas as pd 
import sys 

# Récupération du XCom passer en paramètre
start = sys.argv[1]

# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/logs/logs_xcom_merged.csv") :

	logs = pd.read_csv("/opt/airflow/dags/logs/logs_xcom_merged.csv", sep = ";", decimal = ',')

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[]
	})




# Logs d'échec
new_logs = pd.DataFrame({
	"task_id" : ["echec_execute_python_merged"],
	"start" : [start],
	"end" : [pd.NA],
	"status" : "failed"})


# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])


# Exports
all_logs.to_csv("/opt/airflow/dags/logs/logs_xcom_merged.csv", sep = ';' , decimal = ',', index = False)