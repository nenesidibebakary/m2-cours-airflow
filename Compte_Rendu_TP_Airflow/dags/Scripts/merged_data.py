# -*- coding: utf-8 -*-
"""
Created on Thu May 16 15:20:38 2024

@author: ilis-data-science-1
"""

from datetime import datetime


# Date de début de tâche
start = datetime.now()

import os
import pandas as pd 


# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/logs/logs_xcom_merged.csv") :

	logs = pd.read_csv("/opt/airflow/dags/logs/logs_xcom_merged.csv", sep = ";")

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[],
		"status" : [],
		"Nb_rea_full_previous" : [],
		"Nb_inserted_rea_full_last" : []
	})



# Tâche principale

############################################################################################################################################################################################################################


# data load
chemin_input = '/opt/airflow/dags/DEV/input' 

#chemin_input = 'C:/Users/ilis-data-science-1/airflow/dags/DEV/input' 

folder = [name for name in os.listdir(chemin_input)] # list of folders 
folder.sort()
previous_folder = folder[-2] # select previous folder
max_folder = max([name for name in os.listdir(chemin_input)]) # select the last folder

rea_full_previous = pd.read_csv(f"/opt/airflow/dags/DEV/output/latest_full_export/rea_full_{previous_folder}.csv", sep=";")
rea_month_last = pd.read_csv(f"/opt/airflow/dags/DEV/output/clean_monthly_export/rea_month_{max_folder}.csv", sep=";")

# merged
rea_full_last = pd.concat([rea_full_previous, rea_month_last])

# saved
rea_full_last.to_csv(f"/opt/airflow/dags/DEV/output/latest_full_export/rea_full_{max_folder}.csv", sep=";")


############################################################################################################################################################################################################################


end = datetime.now()

Nb_rea_full_previous = len(rea_full_previous)
Nb_inserted_rea_full_last = len(rea_full_last)

# Logs de l'exécution
new_logs = pd.DataFrame({
	"task_id" : ["execute_python_merged"],
	"start" : [start],
	"end" : [end],
	"status" : ["succes"],
	"Nb_rea_full_previous" : [Nb_rea_full_previous],
	"Nb_inserted_rea_full_last" : [Nb_inserted_rea_full_last]
	})


# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])


# Exports
all_logs.to_csv("/opt/airflow/dags/logs/logs_xcom_merged.csv",
	sep = ';', index = False)


