# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:33:49 2024

@author: Nene sidibe BAKARY
"""

import pandas as pd 
from dateutil.parser import parse
import os
from pathlib import Path
from datetime import datetime


# Date de début de tâche
start = datetime.now()


# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/logs/logs_xcom_aggr.csv") :

	logs = pd.read_csv("/opt/airflow/dags/logs/logs_xcom_aggr.csv", sep = ";")

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[],
		"status" : [],
        "Nb_lines_input_med" : [],
        "Nb_lines_input_sej" : [],
        "Nb_lines_input_pat" : [],
        "Nb_lines_inserted_rea_month" : []
    })
	

# Tâche principale

############################################################################################################################################################################################################################

# environnements
chemin_input = "/opt/airflow/dags/DEV/output/clean_all_files_export"

# data load
max_folder = max([name for name in os.listdir(chemin_input)]) # select the last folder
pathlist = list(Path(chemin_input +"/"+ max_folder).glob("**/*.csv")) # list all files from this folder
data_dict = {filepath.name.split(".csv")[0]:pd.read_csv(filepath, sep=";") for filepath in pathlist } # create a dictionnary with file_nam as key and data of each of them as values
# files lecture
medicament_formated = data_dict["".join([key for key in data_dict.keys() if key.startswith("medicament")])]
sejour_formated = data_dict["".join([key for key in data_dict.keys() if key.startswith("sejour")])]
patient_formated = data_dict["".join([key for key in data_dict.keys() if key.startswith("patient")])]

##############################################################################################################
##### Table rea_month
###################
# Nombre de drogues total et nombre de drogue unique pour chaque ligne
### On utilisera la colonne NOM_DROGUE vu qu'il y a bcp de nan dans la colonne ID_DROGUE

# Groupby sur 'ID_SEJOUR' et calcul du nombre de médicaments uniques et le nombre total de médicaments par ID de séjour
rea_month = medicament_formated.groupby('ID_SEJOUR').agg(nb_medoc_unique=('NOM_DROGUE', 'nunique'), nb_medoc_total=('NOM_DROGUE', 'size')).reset_index()

rea_month= rea_month.merge(sejour_formated[["ID_SEJOUR","ID_PATIENT", "duree_reveil_tot",
                                                                      "duree_rea_tot"]], on = "ID_SEJOUR")

rea_month= rea_month.merge(patient_formated[["ID_PATIENT", "SEXE"]], on = "ID_PATIENT")

rea_month= rea_month.rename(columns={'nb_medoc_total': 'nb_medic', 'SEXE': 'sexe'})
rea_month = rea_month[['ID_SEJOUR','duree_reveil_tot', 'duree_rea_tot', 'nb_medic', 'sexe']]

# saved 
i = max_folder
rea_month.to_csv(f"/opt/airflow/dags/DEV/output/clean_monthly_export/rea_month_{i}.csv", sep=";", index= False)

############################################################################################################################################################################################################################

end = datetime.now()

Nb_lines_inserted_med = len(medicament_formated)
Nb_lines_inserted_sej = len(sejour_formated)
Nb_lines_inserted_pat = len(patient_formated)
Nb_lines_inserted_rea_month = len(rea_month)

# Logs de l'exécution
new_logs = pd.DataFrame({
	"task_id" : ["execute_aggregate"],
	"start" : [start],
	"end" : [end],
	"status" : ["succes"],
    "Nb_lines_input_med" : Nb_lines_inserted_med,
    "Nb_lines_input_sej" : Nb_lines_inserted_sej,
	"Nb_lines_input_pat" : Nb_lines_inserted_pat,
    "Nb_lines_inserted_rea_month" : Nb_lines_inserted_rea_month
    })

# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])

# Exports
all_logs.to_csv("/opt/airflow/dags/logs/logs_xcom_aggr.csv",
	sep = ';', index = False)

